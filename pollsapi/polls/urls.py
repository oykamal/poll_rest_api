from django.urls import path, include
# from . import views
from . import apiviews
from rest_framework.authtoken import views

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('polls', apiviews.PollViewSet, basename='polls')

urlpatterns = [
    path('login/',views.obtain_auth_token),
    path('user/', apiviews.UserCreate.as_view()),
    path('polls/<int:pk>/choices/', apiviews.ChoiceList.as_view(), name="Choice_List"),
    path('polls/<int:pk>/choices/<int:choice_pk>/vote/', apiviews.CreateVote.as_view(), name="Create_Vote"),
    
]

urlpatterns += router.urls
from rest_framework import serializers
from .models import Poll, Choice, Vote
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rest_framework.authtoken.models import Token

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','email', 'password')
        extra_kwargs = {'password':{'write_only':True}}
        
    def create(self, validated_data):
            # password = validated_data.pop('password')
        user = User(
            email = validated_data.get('email'),
            username = validated_data.get('username'),
            password = make_password(validated_data.get('password'))
        )
            # user.set_password(password)
        user.save()
        Token.objects.create(user=user)
        return user

class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = '__all__'
        
        
class ChoiceSerializer(serializers.ModelSerializer):
    votes = VoteSerializer(many= True, required=False)
    
    class Meta:
        model = Choice
        fields = '__all__'


class PollSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(many=True, read_only=True, required=False)
    
    class Meta:
        model = Poll
        fields = '__all__'
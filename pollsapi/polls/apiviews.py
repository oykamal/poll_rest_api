from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import status
from .models import Poll, Choice
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from .serializer import PollSerializer, ChoiceSerializer, VoteSerializer, UserSerializer
from django.contrib.auth import authenticate
from rest_framework.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404

class LoginView(APIView):
    """comment"""
    permission_classes =(AllowAny,)
    def post(self, request):
        # permission_classes =(AllowAny,)
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            return Response({'token':user.auth_token.key})
        return Response({'error':'Wrong cantidate'}, status=status.HTTP_400_BAD_REQUEST)

class UserCreate(generics.CreateAPIView):
    """comment"""
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer
    
    
class PollViewSet(viewsets.ViewSet):
    """comment"""
    # queryset = Poll.objects.all()
    # serializer_class = PollSerializer
    
    def get_queryset(self):
        queryset = Poll.objects.all()
        return queryset

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset,pk=self.kwargs['pk'])
        print(obj)
        return obj
    
    def list(self, request):
        queryset = self.get_queryset()
        serializer = PollSerializer(queryset, many=True)
        return Response(serializer.data)
    
    
    def retrieve(self, request, **kwargs):
        obj = self.get_object()
        serializer = PollSerializer(obj)
        return Response(serializer.data)
    
    def destroy(self, request, **kwargs):
        obj = self.get_object()
        serializer = PollSerializer(obj)
        serializer.delete()
        return Response("deleted")
        
    
    
class ChoiceList(generics.ListCreateAPIView):
    """comment"""
    def get_queryset(self):
        queryset = Choice.objects.filter(poll_id=self.kwargs['pk'])
        return queryset
    serializer_class = ChoiceSerializer


class CreateVote(APIView):
    """comment"""
    serializer_class = VoteSerializer
    
    def post(self, request, pk, choice_pk):
        voted_by = request.data.get('voted_by')
        data = {'choice':choice_pk,
                'poll':pk,
                'voted_by':voted_by}
        serializer = VoteSerializer(data=data)
        if serializer.is_valid():
            vote = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    